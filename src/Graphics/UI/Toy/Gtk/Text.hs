-----------------------------------------------------------------------------
-- |
-- Module      :  Graphics.UI.Toy.Gtk.Text
-- Copyright   :  (c) 2013 Michael Sloan
-- License     :  BSD-style (see the LICENSE file)
--
-- Maintainer  :  Michael Sloan <mgsloan@gmail.com>
-- Stability   :  experimental
-- Portability :  GHC only
--
-- Utilities for drawing text.
--
-----------------------------------------------------------------------------
module Graphics.UI.Toy.Gtk.Text
  ( preText
  , textLineBoundedBaseline
  , textLineBounded
  ) where

import Diagrams.Backend.Cairo (Cairo)
import Diagrams.Backend.Cairo.Text
import System.IO.Unsafe (unsafePerformIO)
import Diagrams.Prelude
import Diagrams.TwoD.Text
import Graphics.UI.Toy.Diagrams

-- | Use cairo to render bounded monospace text, handling newlines properly.
preText :: String -> Diagram Cairo
preText = vcat . map (textLineBoundedBaseline monoStyle) . lines

textLineBoundedBaseline :: Style V2 Double -> String -> Diagram Cairo
textLineBoundedBaseline s = textLineBounded s . Text mempty BaselineText

textLineBounded :: Style V2 Double -> Text Double -> Diagram Cairo
textLineBounded s t = unsafePerformIO $ textLineBoundedIO s t
