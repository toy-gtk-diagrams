{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}
module Examples.Button where

import Data.Default
import Graphics.UI.Toy.Gtk.Prelude

type State = Transformed (Button Cairo V2 Double)

instance GtkDisplay State where display = defaultDisplay

main = runToy (def :: State)

instance Default State where
  def = translate (V2 100 100) $ vcat' (with & sep .~ 10) [but, but, but, but]
   where
    but = mkTransformed (mkDefaultButton "button")
